# AC/BD AWS Condolidated Billing Driller

## Screnshots
Show usage by accounts

![image](https://bitbucket.org/j3tm0t0/aws-consolidated-billing-driller/raw/820918b9c77ab666c83f4e1a2db431f52d99ca14/img/screenshot1.png)

By clicking one slice, Show usage by product, by the user.

![image](https://bitbucket.org/j3tm0t0/aws-consolidated-billing-driller/raw/820918b9c77ab666c83f4e1a2db431f52d99ca14/img/screenshot2.png)

## Working sample
**[ http://acbd.s3w.jp ](http://acbd.s3w.jp/)**

## Usage
- create an IAM account with minimum required policy.

```
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:GetObject"
      ],
      "Resource": [
        "arn:aws:s3:::bucketname/111111111111-aws-billing-csv*"
      ]
    }
  ]
}
```
- enable CORS setting of your bucket like following.

```
<?xml version="1.0" encoding="UTF-8"?>
<CORSConfiguration xmlns="http://s3.amazonaws.com/doc/2006-03-01/">
    <CORSRule>
        <AllowedOrigin>http://acbd.s3w.jp</AllowedOrigin>
        <AllowedMethod>GET</AllowedMethod>
        <AllowedHeader>*</AllowedHeader>
    </CORSRule>
</CORSConfiguration>
```
- fill in fields above. all data is stored only in localStorage and never sent to anywhere.
- push 'Go' and wait until charts are displayed.
- click on slice to drill down, change month or grouping, etc...
- **enjoy!**
