var header=[];
var cache={};
$(function () {

	_.each(['accountid','accesskey','secretkey','bucket'],function(key){
		$('#'+key).val(localStorage[key] || "");
	});
	$('#month').val(getThisMonth());

	$(".monthPicker").datepicker({ 
		dateFormat: 'mm-yy',
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true,
 
		onClose: function(dateText, inst) {  
			var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val(); 
			var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val(); 
			$(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
		}
	});
 
	$(".monthPicker").focus(function () {
		$(".ui-datepicker-calendar").hide();
		$("#ui-datepicker-div").position({
			my: "center top",
			at: "center bottom",
			of: $(this)
		});    
	});

});

function getData()
{
	var bucket,csv,key;
	_.each(['accountid','accesskey','secretkey','bucket'],function(key){
		localStorage[key]=$('#'+key).val();
	});
	AWS.config.update({accessKeyId: $('#accesskey').val(), secretAccessKey: $('#secretkey').val()});
	key=$('#accountid').val()+"-aws-billing-csv-"+$('#month').val()+".csv";
	if (cache[key])
	{
		$('#container').html('<p>processing data</p>');
		drawGraph(sumData(cache[key],{sum:"TotalCost",groupBy:$('#group').val().split(/ - /),where:{field:"RecordType",value:"LinkedLineItem",sort:"desc"}}));
	}
	else
	{
		$('#container').html('<p>fetching s3://'+$('#bucket').val()+"/"+key+' ...</p>');
		console.time('download');
		bucket = new AWS.S3({ params: {Bucket: $('#bucket').val()} });
		bucket.getObject({Key:key},function(err,data){
			console.timeEnd('download');
			csv=data.Body.toString();
			cache[key]=processCsv(csv);
			drawGraph(sumData(cache[key],{sum:"TotalCost",groupBy:$('#group').val().split(/ - /),where:{field:"RecordType",value:"LinkedLineItem",sort:"desc"}}));
		});
	}
}

function processCsv(csv)
{
	console.time('processCsv');
	var data=[];
	$('#csv').text(csv);
	var lines=csv.split(/\n/);
	header=lines.shift().split(/,/);
	_.each(lines,function(line){
		line=line.replace(/([A-z0-9\ ]),([A-z0-9\ ])/g,"$1$2").replace(/\"/g,'');
		var cols=line.split(/,/);
		var entry={};
		$.each(cols,function(j){
			entry[header[j]]=cols[j];
		});
		data.push(entry);
	});
	console.timeEnd('processCsv');
	return data;
}

function sumData(data,option)
{
	var tmp={},total=[],drillData=[],grandTotal=0;
	option = option || {sum:"TotalCost",groupBy:["LinkedAccountName",],where:{field:"RecordType",value:"AccountTotal"},sort:"desc"};

	console.time('sumData');
	
	_.map(data,function(d){
		if( (option.where == null ) || (d[option.where.field] == option.where.value) )
		{
			var pk=d[option.groupBy[0]]; // primary key
			var sk=d[option.groupBy[1]]; // sub key
			if(tmp[pk])
			{
				tmp[pk].sum+=( parseFloat(d[option['sum']]) || 0 ); 
				if(tmp[pk].sub[sk])
				{
					tmp[pk].sub[sk]+=( parseFloat(d[option['sum']]) || 0 ); 
				}
				else
				{
					tmp[pk].sub[sk]=( parseFloat(d[option['sum']]) || 0 ); 
				}
			}
			else
			{
				tmp[pk]={};
				tmp[pk].sum=( parseFloat(d[option['sum']]) || 0 ); 
				tmp[pk].sub={};
				tmp[pk].sub[sk]=( parseFloat(d[option['sum']]) || 0 ); 
			}
		}
	});

	var tsv=option.groupBy.join("\t")+"\t"+option['sum']+"\n";

	var pks=Object.keys(tmp);
	pks=_.sortBy(pks,function(k){return tmp[k].sum*( (option['sort']=='desc')? 1:-1) });

	tsv+=_.map(pks,function(pk){
		total.push({name:pk, y:tmp[pk].sum, drilldown:pk});
		grandTotal+=tmp[pk].sum;
		var sks=Object.keys(tmp[pk].sub);
		var subtotal=[];
		sks=_.sortBy(sks,function(sk){
			return tmp[pk].sub[sk]*( (option['sort']=='desc')? 1:-1);
		});
		var line=_.map(sks,function(sk){
			subtotal.push([sk,tmp[pk].sub[sk]]);
			return pk+"\t"+sk+"\t"+tmp[pk].sub[sk];
		}).join("\n");
		drillData.push({id:pk, name:pk, data:subtotal});
		return line;
	}).join("\n");
	$('#tsv').text(tsv);

	console.timeEnd('sumData');
	return {total:total,drillData:drillData,grandTotal:Math.floor(grandTotal)};
}

function drawGraph(sum)
{
	// Create the chart
	$('#container').highcharts({
		chart: {
			type: 'pie',
			width: '1024',
			height: '600'
		},
		title: {
			text: "Total "+addComma(sum.grandTotal)+" USD"
		},
		subtitle: {
			text: 'Click slices to drill down.'
		},
		plotOptions: {
			series: {
				dataLabels: {
					enabled: true,
					format: '{point.name}: {point.y:,.1f} USD'
				}
			}
		},

		tooltip: {
			headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
			pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:,.1f}</b>USD<br/>'
		}, 

		series: [{
			name: 'PrimaryKey',
			colorByPoint: true,
			data: sum.total
		}],
		drilldown: {
			series: sum.drillData
		}
	})

}
