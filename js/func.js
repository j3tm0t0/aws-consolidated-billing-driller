_.sum = function(obj) {
  if (!$.isArray(obj) || obj.length == 0) return 0;
  return _.reduce(obj, function(sum, n) {
    return sum += n;
  });
};

function getThisMonth(){
	var d=new Date();
	var month=d.getUTCMonth()+1;
	month=(month<10)? "0"+month:month;
	return d.getUTCFullYear()+"-"+month;
}

function addComma(num){
	return String( num ).replace( /(\d)(?=(\d\d\d)+(?!\d))/g, '$1,' );
}